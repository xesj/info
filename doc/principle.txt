+-----------+
| PRINCIPLE |
--------------------------------------------------------------------------------------------------------------------------------
IHL

- Az egymásra hivatkozó html fájlok sosem nyitnak új lapot, így tehát egyetlen "információs html lap" (IHL) van.
  Ha viszont külső lapokra hivatkozunk linkekkel, azok mindig új lapon kell hogy megnyíljanak, hogy ne veszítsük el
  az egyetlen információs html lapot.
--------------------------------------------------------------------------------------------------------------------------------
TEXT/PLAIN SCRIPT

- Mindig hagyjunk egy üres nyitó, és egy üres záró sort, hogy jól elkülönüljön a html-től beidézett kódrészlet:

    <script type="text/plain">
          
    Valami kódrészlet vagy hasonló szöveg.        

    </script>
--------------------------------------------------------------------------------------------------------------------------------