+----------------------+
| INTELLIJ 2020.3 INFO |
--------------------------------------------------------------------------------------------------------------------------------
BUG / HÜLYE MŰKÖDÉS

- Windows alatt többször elszállt az Intellij a 11.0.2-es JDK-tól, ezért beállítottam helyette a 11.0.9-est.

- Maven projekt esetén is feleslegesen hoz létre ".gradle" könyvtárat.

- Létrehozza a ".idea" könyvtárat, ami alatt nagyon sok fájl van, de zavaros hogy ezek közül melyeket kell
  letiltani a ".gitignore" fájlban hogy több fejlesztő is tudjon egy projektet fejleszteni, merge konfliktus nélkül.

- Új Intellij verzióra frissítve, illetve néha egy-egy projektnél beáll a teljes káosz, az Intellij elveszti
  az uralmát a projekt felett, nem ismeri fel. Ez a legsúlyosabb probléma!
  - A projekt beállításai elvesznek
  - Nem ismeri fel hogy maven projekt
  - Nem ismeri fel a projekt struktúrát, nem tudja hol a forráskód, hol a resource, ...
  - A projekt nézet (scope) elveszik
  - A java osztályokat nem érzékeli java forráskódnak, nem működik semmi a kódon (se help, se api doc), 
    mintha csak txt fájlok lennének.
  - Más verziójú SDK-val akarja fordítani
  - Nem tudja milyen könyvtárba kell rakni a lefordított programkódot
  - stb.

--------------------------------------------------------------------------------------------------------------------------------
- Dokumentációk:
  Project tool window -> https://www.jetbrains.com/help/idea/project-tool-window.html
  Auto compile -> https://stackoverflow.com/questions/12744303/intellij-idea-java-classes-not-auto-compiling-on-save

- Az IJ futtatásához legalább 11-es JDK szükséges, ennek helyének beállítása egy környezeti változóval lehet: IDEA_JDK_64
  Az intellij installáláskor már van ilyen környezeti változó, de lehet rossz helyre mutat

- Preferences beállítások mentése, visszaállítása.
  !!! FONTOS, HOGY EGY MENTÉST NE HASZNÁLJUK VISSZAÁLLÍTÁSRA MÁS OPERÁCIÓS RENDSZEREN, MERT ELRONTJA AZ INTELLIJ-T !!!
  File -> Manage IDE settings -> Export Settings / Import Settings

- Fontos toolbar-ok megjelenítése:
  View -> Appearance -> Toolbar: ON
                        Tool Window Bars: ON
                        Status Bar: ON
                        Details in Tree View: OFF (Project ablakban ne látszódjon a fájlok mellett a módosítás dátuma)

- Megnyitott fájlok füleslap (EDITOR TABS) beállításai,
  például hogy a megnyitott fájlok listája bal oldalon legyen, nagy betűvel írja ki a fájlnevelet, ...:
  Preferences -> Editor -> General -> Editor Tabs

- Projekt ablakban egy jó elrendezés: a lenyíló listából a "Project" helyett a "Packages" -t válasszuk,
  mert akkor csak ezeket mutatja:
  - /src/main/java package-ek
  - /src/main/resources könyvtárak

- Autosave, vagyis minden fájl szerkesztésekor automatikus mentés történjen a fájlrendszerbe:
  Preferences -> System Settings -> Autosave: ON (mind a 4 checkbox)
  A még nem mentett fájl jelzése "*" karakterrel:
  Preferences -> Editor -> General -> Editor Tabs -> Mark modified (*): ON

- Fejlesztés hot deploy: csak azt állítsuk be hogy F9-re build töténjen:
  Preferences -> Keymap -> Build Project = F9
  A "build" funkció az intellij belső "Ant build"-jét jelenti, nem a maven build-et!
  Így a programon módosítása után csak az F9-et kell lenyomni, mert a build csak a módosított fájlokat fordítja újra,
  a spring ezt észreveszi, és újraindul az alkalmazás.

- Beállíthatók fájlok és könyvtárak, melyek nem jelennek meg az IDE-ben:
  Preferences -> Editor -> File Types -> Ignored Files or Folders

- A ".properties" fájlok helyesen jelenjenek meg, nem pedig \unnnn karakterekkel:
  Preferences -> Editor -> File Encodings -> Transparent native-to-ascii conversion: ON

- Smart home letiltása, a HOME billentyűvel a kurzor a sor elejére ugorjon:
  Preferences -> Editor -> General -> Smart Keys -> Home moves caret to first non-whitespace character: OFF

- Intellij memória használat maximum beállítás:
  Help -> Edit Custom VM Options
  Ez szerkeszti az idea.vmoptions fájlt. Írjuk át az Xmx értékét 2 gigabájt-ra:
  -Xmx2g
  és utána indítsuk újra az Intellij-t, hogy érvényre jusson.

- Intellij memória használat megjelenítése: a képernyő legalján lévő "status bar"-ban tud megjelenni.
  A "status bar"-on nyomjunk egér jobbgombot, és: Memory Indicator: ON

- Spring Boot Jar előállítása:
  A "maven" ablakban, válasszuk a "package" lifecycle-t, és "run maven build" (zöld háromszög)

- "Actions for url" kisablak, és egyéb hint-ek letiltása:
  Preferences -> Editor -> Inlay Hints -> Show hints for: OFF (minden checkbox-ra)

- Forráskód mellett balra a gutter-en (ahol a forráskód száma is van) ne jelenjenek meg ikonok:
  Preferences -> Editor -> General -> Gutter Icons -> Show gutter icons: OFF

- Projekthez tartozó külső library-k source/javadoc részeinek letöltése:
  Nyissuk meg a maven fület, és ebben a maven ablakban felül található egy gomb:
  "Download Sources and/or Documentation"

- Forráskód documentation:
  Állítsuk be hogy "ctrl + space" billentyűre jelenjenek meg a metódusok,
  "F1" billentyűre jelenjen meg az api dokumentáció,
  "shift + F1" billentyűre pedig az api dokumentáció egy web böngészőben jelenjen meg:
  Preferences -> Keymap -> Quick documentation = F1
  Preferences -> Keymap -> External documentation = Shift+F1
  Alapból F1-et is nyomhatunk a forráskódon (ctrl + space előtt), ekkor is kaphatunk segítséget.
  Tiltsuk le a dokumentáció megjelenítését az egér forráskód feletti mozgatásakor:
  Preferences -> Editor -> Code Editing -> Show quick documentation on mouse move: OFF

- Összeállítható egy speciális nézet a "project" ablakban, például "my-scope" névvel.
  Nagyon hasznos mert összeválogatható a projekt mely fájljai, könyvtárai látszódjanak,
  ha a project ablakban a scope-ot választjuk:
  Preferences -> Appearance & Behavior -> Scopes

- Téma beállítása:
  Preferences -> Appearance -> Theme -> IntelliJ Light

- Szinek beállítása a forráskód editorban: előszőr ezzel kezdjük, ne pedig mély szinten (ne a java-nál):
  Preferences -> Editor -> Color Scheme -> Language Defaults
  Beállítás:
  - Comment color: DC0000
  - Sehol ne legyenek beállítva: BOLD, ITALIC, EFFECTS

- A forráskód editorban ne látszódjon más háttérszínnel az a kulcsszó amin a kurzor áll (mark occurences):
  Preferences -> Editor -> Code Editing -> Highlight on Caret Movement -> Usages of element at caret: OFF

- Editor színséma, szinek beállítása, például az aktuális sornak (Caret row) ne legyen más háttérszíne:
  Preferences -> Editor -> Color Scheme -> General
  Editor háttérszín beállítás:
  Preferences -> Editor -> Color Scheme -> General -> Text -> Default text -> Background

- Forráskód hibák (javaslatok) az editorban (intentions):
  Ezeket sárga izzó (javaslat), vagy piros izzó (hibajavítás) jelzi.
  A sárga izzó általában felesleges javaslat, érdemes letiltani, de vele a piros izzó is letiltásra kerül:
  Preferences -> Editor -> General -> Appearance -> Show intention bulb: OFF
  Ezután ha a forráskódban hiba van, azt más szinnel jelzi az editor. A hiba megjelenik ha a hibás szövegre toljuk az egeret,
  vagy a "Problems" ablakban kattintgatunk a hibalistán. Ha rátoljuk az egeret, akkor a "More actions" link,
  mutatja a hiba lehetséges elhárítását. Ha a szövegen Alt+Enter -t nyomunk, akkor egyből a hibaelhárításhoz jutunk!
  Ha a forráskódban hiba van, akkor alapból a szöveg pirosan jelenik meg, de ez nem jó, mert a kommentet is
  piros színre állítottuk. Ezért cseréljük ki a hibajelzést piros aláhúzásra:
  Preferenes -> Editor -> Color Scheme -> General -> Errors and Warnings:
      Error, Unknown symbol:
        Foreground: OFF
        Effects: FF0000 + Underwaved
      Unused Symbol:
        Effects: FFDD00 + Underwaved

- Toolbar módosítása, "Undo", "Redo", "Reformat Code" műveletek hozzáadása:
  Preferences -> Appearance & Behavior -> Menus and Toolbars -> Main Toolbar
    -> Álljunk arra az elemre, ami alá be akarjuk illeszteni az új funkciót,
       és az új funkciót válasszuk ki a "+(Add Action)" gombbal megjelenő listából.

- Tabulátor, bekezdés méret-beállítások:
  Preferences -> Editor -> Code Style -> Java, Html, Javascript, Style Sheets(Css) ->
    Tab size: 2
    Indent: 2
    Continuation indent: 2

- Forráskód formázáskor ne tegyen be a kódba üres sorokat:
  Editor -> Code Style -> Java -> Blank Lines -> Minimum blank lines: 0-ra állítani az értékeket
  Editor -> Code Style -> Java -> Imports -> Import Layout: a táblázatból törölni kell a <blank line> sorokat.

- Az import ne használjon "*"-os importokat:
  Editor -> Code Style -> Java -> Imports -> Class count to use import with '*': 999
  Editor -> Code Style -> Java -> Imports -> Names count to use static import with '*': 999
  (a mezőket nem hagyhatjuk üresen, mert visszaírja bele a default 5 és 3 értéket)

- A java osztályok blokk kommentjeit ne változtassa meg vizuálisan (egy sorba, piros széllel):
  Editor -> General -> Appearance -> Render documentation comments: OFF

- Font beállítás
  A MacOs-en futó Intellij font listája alapból tartalmazza a "JetBrains Mono" fontot, ami nagyon jól néz ki.
  A Windows viszont nem tartalmazza, ezért telepíteni kell. A telepítéshez a fontot le kell tölteni innen:
  https://www.jetbrains.com/lp/mono/
  Ki kell csomagolni a letöltött zip-fájlt, és a "ttf" könyvtárában lévő összes fájlra kattintani,
  majd a "Telepítés" gombokat végignyomogatni, hogy települjön a windows alá.
  Ezután indítsuk újra az Intellij-t, majd kiválasztható a font:
  Preferences -> Editor -> Font ->
    Font: JetBrains Mono
    Size: 15
    Line spacing: 1.0

- Inspections beállítás: Preferences -> Editor -> Inspections
  Azért fontos, hogy jól legyen beállítva, mert ha mindent kikapcsolunk, akkor túl kevés dologért fog szólni.
  Javasolt beállítás a Preferences -> Editor -> Inspections menün belül:
    Szóljon azért, mert felesleges importokat használunk a java osztályokban:
      Java -> Imports -> Unused import: ON
    Ne szóljon a hibás helyesírásért:
      Proofreading: OFF

- Intellij-ben nincs olyan mint a Netbans-ben hogy "Project properties".
  Helyette itt lehet beállítani néhány dolgot a projekthez:
  File -> Project Structure (toolbar ikonként is megjelenik)
  Itt állítható be a projekthez rendelt application server, az SDK, ...
  A futtatás konfigurációját viszont itt kell beállítani:
  Run -> Edit Configurations (toolbar ikonként is megjelenik)

- Ne jelenjenek meg a metódusok, ciklusok elejét és végét összekötő függőleges vonalak:
  Preferences -> Editor -> General -> Appearance -> Show indent guides: OFF

- Verziókezelés beállítása:
  Preferences -> Version Control -> Commit -> "az összes checkbox" -> OFF

- Project Files nézetben bizonyos fájlokat, illetve könyvtárakat el lehet rejteni. Ennek beállítása:
  Preferences -> Editor -> File Types -> Ignored Files or Folders
  Továbbá ezt is be kell állítani:
  Preferences -> Build,Execution,Deployment -> Build Tools -> Maven -> Ignored Files -> ne legyen semmi bejelölve

--------------------------------------------------------------------------------------------------------------------------------
HASZNÁLAT

- Ha a GIT nem látja a távoli repository módosításait, akkor a git-ablakban megjelnő "Fetch All Remotes" ikonnal frissíthatő.

- Kereső funkciók:
  "Search Everywhere" (nagyító ikon)  ->  Projekt fájlok neveiben keres
  "Find in files" (ctrl + shift + f)  ->  Projekt fájlok tartalmában keres
  "Find" (ctrl + f)                   ->  Aktuális fájlban keres

- A forráskódon többször nyomott F1 billentyű popup ablakban, és külön ablakban is meg tudja jeleníteni a dokumentációt,
  attól függően hányszor nyomjuk meg. Ezt a működést nem tudtam megváltoztatni "csak popup ablakos" megjelenítésre.

- Event log jelzés: ez is legyen a bal alsó sarokban mert lehetséges, hogy egy git-es "Update Project" művelet
  nem tud sikeresen lezajlani, és erről az event log piros jelzése fog tájékoztatni, vagy pedig sikeresen lefut,
  és akkor zöld jelzésű event log lesz látható.

- Fájlok szinei
  - Piros: nincsenek a verziókezelő által kezelve
  - Szürke: le vannak tiltva a ".gitignore" fájlban, ezért nem vesznek részt verziókezelő műveletben
  - Fekete: verziókezelt, de nem változtak az utolsó commit óta
  - Zöld: verziókezelt új fájl, mely még nincs benne egyetlen commit-ban sem
  - Kék: verzókezelt módosított fájl, a tartalma eltér a commit-ált állapottól

- Unit test
  Ha van olyan teszt ami nem fut le, attól még lehet a projektet build-elni (f9), és futtatni.
  Csak a maven "package" lifecycle nem fut le, mert az már magába foglalja a sikeres tesztet is.

- A project alatt lévő ".gradle" könyvtárat töröljük le, semmi szükség nincs rá.

- Egységes keymap, operációs rendszer függetlenül (Beállítás: Preferences -> Keymap)
  ctrl + c          =  Copy
  ctrl + v          =  Paste
  ctrl + x          =  Cut
  ctrl + a          =  Select All
  ctrl + r          =  Rename
  ctrl + u          =  Undo
  ctrl + /          =  Comment, uncomment
  f1                =  Quick documentation
  ctrl + space      =  Main Menu / Code / Code Completion / Basic
  ctrl + f          =  Find
  ctrl + g          =  Find Next
  ctrl + shift + g  =  Find Previous
  ctrl + shift + f  =  Find in Files
  f8                =  Save All
  f9                =  Build Project
  f10               =  Run

- A ".gitignore" helyes beállítása, ezeket ki kell zárni a verziókezelőből:

    target/
    out/
    *.iml
    *.iws
    .idea/workspace.xml
    .idea/tasks.xml
    .idea/usage.statistics.xml
    .idea/dictionaries/
    .idea/shelf/
    .idea/libraries/
    .idea/modules.xml

  A ".gitignore" beállításának leírása:
  https://intellij-support.jetbrains.com/hc/en-us/articles/206544839-How-to-manage-projects-under-Version-Control-Systems
--------------------------------------------------------------------------------------------------------------------------------
