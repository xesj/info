package xesj.app.rabbit;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import xesj.app.system.MainApplication;
import xesj.tool.ThreadTool;

/**
 * Consumer service.
 * Időnként új channel-t nyit, így leszakadás esetén újra tud kapcsolódni
 * A MainApplication.runConsumer alapján elindítja, vagy leállítja a consumer-t. 
 */
@Service
public class ConsumerService {
  
  @Autowired MainApplication application;
  
  /**
   * Scheduled
   */
  @Scheduled(fixedDelay = 1)
  public void scheduled() throws IOException, TimeoutException {
    
    // RunConsumer vizsgálat 5 másodpercenként
    while (!application.isRunConsumer()) {
      ThreadTool.sleep(5000);
    }
    
    // Rabbit kapcsolat nyitás
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(RabbitConstant.HOST);
    factory.setPort(RabbitConstant.PORT);
    factory.setVirtualHost(RabbitConstant.VIRTUAL_HOST);
    factory.setUsername(RabbitConstant.USER);
    factory.setPassword(RabbitConstant.PASSWORD);
    try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
      System.out.println("channel open");
      
      // Consumer aktiválás
      ConsumerReader consumerReader = new ConsumerReader(channel);
      String consumerTag = channel.basicConsume(RabbitConstant.QUEUE, false, consumerReader);

      // Várakozás 30 percig, vagy runConsumer=false állapotig
      long t1 = System.currentTimeMillis();
      while (true) {
        ThreadTool.sleep(5000);
        long t2 = System.currentTimeMillis();
        if (!application.isRunConsumer() || t2 - t1 > 30 * 60 * 1000) break;
      }  

      // Consumer leállítás kérése
      channel.basicCancel(consumerTag); 
    }    
    System.out.println("channel close");
    
  }

  // ====  
}
