package xesj.app.rabbit;

/**
 * Rabbit konstansok
 */
public class RabbitConstant {
  
  static final String HOST = "teszt.hu";
  static final int PORT = 5672;
  static final String VIRTUAL_HOST = "vhost-name";
  static final String USER = "joe";
  static final String PASSWORD = "secret";
  static final String QUEUE = "queue-name";
  
}
