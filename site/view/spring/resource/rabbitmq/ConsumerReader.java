package xesj.app.rabbit;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import xesj.tool.ThreadTool;

/**
 * Consumer, mely az üzeneteket olvassa
 */
public class ConsumerReader extends DefaultConsumer {
  
  /**
   * Konstruktor
   */
  public ConsumerReader(Channel channel) {
    
    super(channel);
    
  }
  
  /**
   * Üzenet olvasása
   */
  @Override
  public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) 
    throws IOException {

    // Zárt channel esetén nincs műveletvégzés
    if (!getChannel().isOpen()) {
      return;
    }
     
    // Üzenet olvasása 
    String message = new String(body, StandardCharsets.UTF_8);
    System.out.println("Consumer read message: " + message);

    // Üzenet feldolgozása (várakozás)
    ThreadTool.sleep(3000);

    // ACK (nyitott channel esetén)
    if (getChannel().isOpen()) {
      getChannel().basicAck(envelope.getDeliveryTag(), false);
    }  

  }

  // ====  
}
