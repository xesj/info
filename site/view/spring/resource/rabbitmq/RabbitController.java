package xesj.app.rabbit;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import xesj.app.system.MainApplication;
import xesj.app.system.MainController;
import xesj.app.util.main.Constant;

/**
 * Rabbit controller
 */
@Controller
public class RabbitController {

  public static final String 
    PATH_PRODUCER = "/rabbit/producer",
    PATH_CONSUMER = "/rabbit/consumer";
  
  @Autowired MainApplication application;
  
  /**
   * GET: /rabbit/producer
   */
  @GetMapping(PATH_PRODUCER)
  @ResponseBody
  public String producer() throws Exception {
    
    // Rabbit kapcsolat nyitás
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(RabbitConstant.HOST);
    factory.setPort(RabbitConstant.PORT);
    factory.setVirtualHost(RabbitConstant.VIRTUAL_HOST);
    factory.setUsername(RabbitConstant.USER);
    factory.setPassword(RabbitConstant.PASSWORD);
    try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
      
      // Üzenet küldése adott queue-ra
      String message = "Üzenet időpont: " + LocalDateTime.now(); 
      channel.queueDeclare(RabbitConstant.QUEUE, false, false, false, null);
      channel.basicPublish("", RabbitConstant.QUEUE, null, message.getBytes());      
      
    }    
    
    // Válasz
    return "Rabbit üzenet elküldve";

  }

  /**
   * GET: /rabbit/consumer
   */
  @GetMapping(PATH_CONSUMER)
  public String consumer(@RequestParam boolean run) throws Exception {
    
    // Consumer indítási flag beállítása
    application.setRunConsumer(run);
    
    // Válasz
    return Constant.REDIRECT + MainController.PATH_ROOT;
      
  }
  
  // ====
}
