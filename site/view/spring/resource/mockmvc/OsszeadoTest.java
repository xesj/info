/**
 * Ha MockMvc-t használunk akkor ne használjuk együtt ezzel, hiszen nem akarjuk az embedded servert elindítani:
 * @SpringBootTest(webEnvironment = WebEnvironment.XXX)
 */
package xesj.test;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import xesj.app.system.Run;

// MockMvc imports
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Összeadó teszt
 */
@SpringBootTest(classes = Run.class)
@AutoConfigureMockMvc    // MockMvc használatához kötelező 
public class OsszeadoTest {
  
  @Autowired MockMvc mockMvc;    // MockMvc spring-bean behúzása

  /**
   * Első teszt
   */
  @Test
  public void elsoTest() throws Exception {
    
    // Get hívás query paraméterekkel
    mockMvc.perform(get("/rest/osszeado").param("szoveg", "árvíztűrő").param("a", "6").param("b", "7"))    // Request
      .andDo(print())    // Request-response válasz összes adatának kiírása
      .andExpect(status().isOk())    // 200-as a válasz http-státusza?
      .andExpect(header().string("Cache-Control", "no-store"))    // Header elemének ellenőrzése
      .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))    // "text/plain ..." a válasz content type?
      .andExpect(content().string("árvíztűrő13"));    // A válasz body ez a konkrét érték?
    
    // Post hívás json body-val
    String requestBody = 
      """
      {
        "szoveg": "tükörfúrógép",
        "a": 12,
        "b": 34
      }      
      """;
    
    String expectedResponseBody = 
      """
      {
        "result": "tükörfúrógép46"
      }      
      """;
    
    mockMvc.perform(post("/rest/osszeado").contentType(MediaType.APPLICATION_JSON).content(requestBody))    // Request
      .andDo(print())    // Request-response válasz összes adatának kiírása
      .andExpect(status().isOk())    // 200-as a válasz http-státusza?
      .andExpect(header().string("Cache-Control", "no-store"))    // Header elemének ellenőrzése
      .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))    // "application/json ..." a válasz content type?
      // A válasz json-ben szerepelnek azok a kulcsok és értékek amit ez a json felsorol? A válasz lehet bővebb is!          
      .andExpect(content().json(expectedResponseBody))    
      // Konkrét json elemek lekérdezése, lásd google: "JsonPath Expressions"
      .andExpect(jsonPath("$.result").exists())    // Létezik a "result" kulcs a json gyökerénél?
      .andExpect(jsonPath("$.result").isString())    // A "result" értéke string típusú
      .andExpect(jsonPath("$.result").value("tükörfúrógép46"))    // A "result" értéke pont ez?
      .andExpect(jsonPath("$.ilyennincs[7].abc").doesNotExist());    // Ugye nincs ilyen elem?
    
    // Hozzáférés a teljes response-hoz, ha szükségünk van rá, például máshogy ellenőriznénk
    MvcResult mvcResult = mockMvc.perform(post("/rest/osszeado").contentType(MediaType.APPLICATION_JSON).content(requestBody)) 
      .andExpect(status().isOk())
      .andReturn();    // Ez kell a hozzáféréshez, hogy MvcResult-ot kapjunk. 
    String responseContentType = mvcResult.getResponse().getContentType();    // Response content type
    String responseBodyText = mvcResult.getResponse().getContentAsString();   // Response body
    
  }
  
  // ====
}