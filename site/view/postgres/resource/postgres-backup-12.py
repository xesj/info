#!/usr/bin/env python3
# version: 1.0
import datetime, subprocess

def backup(database):
  file = '/Users/xesj/archiv/postgres-' + database + '-' + today + '.backup'
  print()
  print('"' + database + '" database backup:')
  print('"postgres" user ', end = '', flush = True)
  result = subprocess.run('/Library/PostgreSQL/12/bin/pg_dump -p 5433 -U postgres ' + database + ' >' + file, shell = True)
  if result.returncode == 0:
    print('backup ready:', file)
  else:
    print('BACKUP FAILED !!!')

print('PostgreSQL 12 backup...')
today = str(datetime.date.today())
try:
  backup('eles12')
  backup('fejleszto12')
except KeyboardInterrupt:
  pass