package xesj.app;
import java.util.Arrays;
import java.util.List;

/**
 * Típus hiba bemutatása:
 * Így lehet integer-eket tároló listába stringeket tölteni, majd ezután a lista kezelhetetlenné válik.
 */
public class TypeMismatch {

  /**
   * main()
   */
  public static void main(String[] args) throws Exception {

    // Ez a sor hiba nélkül lefut, az integerList-ben rossz adatok lesznek
    List<Integer> integerList = stringList(List.class);

    // Ez a sor még hiba nélkül lefut, mert az integerList elemét "nem elemezzük", és nem használjuk fel
    integerList.get(0);

    // Szeretnénk kideríteni az integerList egy elemének típusát, de:
    // ClassCastException keletkezik: java.lang.String cannot be cast to class java.lang.Integer
    integerList.get(0).getClass();

    // Szeretnénk kiolvasni az integerList egy elemét, de:
    // ClassCastException keletkezik: java.lang.String cannot be cast to class java.lang.Integer
    Integer i = integerList.get(0);

    // Ezt a sort a compiler nem fogadja el, mert az integerList nem string-eket tartalmaz:
    // String s = integerList.get(0);

  }

  /**
   * Stringek listája
   */
  static <T> T stringList(Class<T> cls) {

    return (T)(Arrays.asList("a", "b", "c"));

  }

  // ====
}
