#!/bin/bash
# +-----------------------------------------------------------------+
# |Figyelem:                                                        |
# |  Ennek a fájlnak a "root" legyen a tulajdonosa.                 |
# |  Erre a fájlra a root-nak legyen olvasási és végrehajtási joga. |
# +-----------------------------------------------------------------+ 
export JAVA_HOME=$(/usr/libexec/java_home -v 13)
cd /Users/xesj/service/gepard
java -jar ./gepard.jar --spring.profiles.active=host-eles,db-eles
