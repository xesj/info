# Saját "bin" könyvtár beállítása a path-ba 
export PATH=~/bin:/usr/local/git/bin:$PATH

# JDK alias beállítások
alias jdk8='export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)'
alias jdk11='export JAVA_HOME=$(/usr/libexec/java_home -v 11)'

# Default JDK beállítása
jdk11

# Python 3.9
PATH="/Library/Frameworks/Python.framework/Versions/3.9/bin:${PATH}"
export PATH

# Általános alias beállítások
alias ll='ls -l'
