/**
 * text/plain script átalakító eljárás, mely trim()-meli a tartalmát.
 */
let items = document.querySelectorAll('script[type="text/plain"]');
for (let item of items) {
  item.innerHTML = item.innerHTML.trim();
};
